================
pygments-graphql
================

`GraphQL <https://graphql.org/>`_ lexer for `Pygments
<https://pygments.org/>`_.
